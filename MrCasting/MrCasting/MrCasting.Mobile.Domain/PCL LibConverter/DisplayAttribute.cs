﻿namespace System.ComponentModel.DataAnnotations
{
    public sealed class DisplayAttribute : Attribute
    {

        public DisplayAttribute()
        {
        }

        public bool AutoGenerateField { get; set; }
        public bool AutoGenerateFilter { get; set; }
        public string Description { get; set; }
        public string GroupName { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }
        public string Prompt { get; set; }
        public Type ResourceType { get; set; }
        public string ShortName { get; set; }

        public bool? GetAutoGenerateField()
        {
            return default(bool?);
        }

        public bool? GetAutoGenerateFilter()
        {
            return default(bool?);
        }

        public string GetDescription()
        {
            return default(string);
        }
        public string GetGroupName()
        {
            return default(string);
        }
        public string GetName()
        {
            return default(string);
        }
        public int? GetOrder()
        {
            return default(int?);
        }
        public string GetPrompt()
        {
            return default(string);
        }
        public string GetShortName()
        {
            return default(string);
        }
    }
}
