﻿using MrCasting.Domain.DTO;
using MrCasting.Domain.Entities;
using MrCasting.Domain.Enuns;
using MrCasting.Domain.ExtendionMethods;
using MrCasting.Domain.Interfaces.Application;
using MrCasting.Services.Helpers;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MrCasting.Services.Controllers
{
    public class LoginOAuthController : ApiController
    {
        private HttpResult<CandidatoDTO> _result;
        private readonly ILoginOAuthAppService service;
        ILoginOAuthAppService Service
        {
            get
            {
                if (service == null)
                    throw new Exception("CandidatoAppService não pode ser nulo");
                return service;
            }
        }

        public LoginOAuthController() { }

        public LoginOAuthController(ILoginOAuthAppService service)
        {
            this.service = service;
            this._result = new HttpResult<CandidatoDTO>();
        }

        [Route("api/LoginOAuth/Login")]
        [HttpPost]
        public HttpResponseMessage RealizarLogin(LoginOAuthDTO loginDTO)
        {
            LoginOAuth login = loginDTO.ToLoginOAuth();
            
            try
            {
                Conta conta = Service.Logar(login);
                if (conta.Tipo == TipoConta.Scouter)
                    throw new HttpResponseException(HttpStatusCode.Moved);
                Candidato candidato = conta.Candidato;
                return _result.Success(candidato.ToCandidatoDTO());
            }
            catch (Exception ex)
            {
                return _result.ReturnCustomException(ex);
            }
        }
    }
}
