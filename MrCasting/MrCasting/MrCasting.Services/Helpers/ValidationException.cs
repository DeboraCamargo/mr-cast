﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MrCasting.Services.Helpers
{
    public class CustomValidationException : Exception
    {
        public IList<ValidationResult> Errors { get; private set; }

        public CustomValidationException(IList<ValidationResult> erros)
        {
            Errors = erros;
        }
        public CustomValidationException(string message) : base(message)
        {
            Errors = new List<ValidationResult> { new ValidationResult(message) };
        }
    }

    public class CustomNotFoundException : Exception
    {
        public CustomNotFoundException(string message)
            : base(message)
        {

        }
    }

    public class CustomBadRequestException : Exception
    {
        public CustomBadRequestException(string message)
            : base(message)
        {

        }
    }

    public class AlreadyExistsException : Exception
    {
        public AlreadyExistsException(string message)
            : base(message)
        {

        }

    }

    public class CustomNotAuthorizedException : Exception
    {
        public CustomNotAuthorizedException(string message)
            : base(message)
        {

        }

    }
}