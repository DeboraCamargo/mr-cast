﻿using MrCasting.Domain.Enuns;
using MrCasting.Domain.DTO;
using MrCasting.Mobile.Model;
using System;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace MrCasting.Mobile.ViewModel
{
    public class CandidatoViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        //private string nome;
        //private string sobrenome;
        private string nomeArtistico;
        private string profissao;
        private string nacionalidade;
        private string naturalidade;
        private string drt;
        private string realise;
        //private string email;
        //private string telefone;

        CandidatoModel model = new CandidatoModel();

        public CandidatoViewModel()
        {
        }

        public string Nacionalidade
        {
            set
            {
                if (nacionalidade != value)
                {
                    nacionalidade = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("Nacionalidade"));
                    }
                }
            }
            get
            {
                return nacionalidade;
            }
        }

        public string Naturalidade
        {
            set
            {
                if (naturalidade != value)
                {
                    naturalidade = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("Naturalidade"));
                    }
                }
            }
            get { return naturalidade; }
        }



        public string NomeArtistico
        {
            set
            {
                if (nomeArtistico != value)
                {
                    nomeArtistico = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("NomeArtistico"));
                    }
                }
            }
            get
            {
                return nomeArtistico;
            }
        }

        public string Profissao
        {
            set
            {
                if (profissao != value)
                {
                    profissao = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("Profissao"));
                    }
                }
            }
            get
            {
                return profissao;
            }
        }

        public string Realise
        {
            set
            {
                if (realise != value)
                {
                    realise = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("Realise"));
                    }
                }
            }
            get { return realise; }
        }


        public string Drt
        {
            set
            {
                if (drt != value)
                {
                    drt = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("Drt"));
                    }
                }
            }
            get { return drt; }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this,
                    new PropertyChangedEventArgs(propertyName));
            }
        }

        private Command cadastrarCommand;
        public ICommand CadastrarCommand
        {
            get
            {
                if (cadastrarCommand == null)
                {
                    cadastrarCommand = new Command(OnCadastrarClicked);
                }

                return cadastrarCommand;
            }
        }

        public async void OnCadastrarClicked(object param)
        {
            PessoaDTO pessoaDTO = new PessoaDTO();
            //{
            //    Nome = nome,
            //    Sobrenome = sobrenome,
            //    Contato = new ContatoDTO()
            //    {
            //        Email = email,
            //        DDD = "11",
            //        Telefone = telefone
            //    },
            //    Cpf = "33550028873",
            //    DataNascimento = DateTime.Today.AddYears(-21),
            //    Sexo = Genero.Feminino,
            //    TokenAlteracaoDeSenha = new Guid()

            //};

            CandidatoDTO candidatoDTO = new CandidatoDTO()
            {
                NomeFantasia = nomeArtistico,
                Profissao = profissao,
                DadosPessoais = pessoaDTO,
                SobrenomeFantasia = string.Empty,
                Naturalidade = string.Empty,
                Nacionalidade = string.Empty,
                DRT = string.Empty,
                Realise = realise

            };


            await model.InsertCandidatosAsync(candidatoDTO);
        }

    }
}
