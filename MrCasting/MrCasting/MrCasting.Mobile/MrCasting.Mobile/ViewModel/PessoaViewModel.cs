﻿using MrCasting.Domain.DTO;
using MrCasting.Domain.Enuns;
using MrCasting.Mobile.Model;
using System;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace MrCasting.Mobile.ViewModel
{
    public class PessoaViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        PessoaModel model = new PessoaModel();
        PessoaDTO pessoaDTO;

        public PessoaViewModel()
        {
            pessoaDTO = (PessoaDTO)NavigationHandler.Session["Pessoa"];
            if (pessoaDTO != null)
            {
                Nome = pessoaDTO.Nome;
                Nascimento = pessoaDTO.DataNascimento;
                Sexo = pessoaDTO.Sexo;
                Sobrenome = pessoaDTO.Sobrenome;
                Cpf = pessoaDTO.Cpf;
                Editando = true;
            }
        }

        private bool editando;
        public bool Editando
        {
            set
            {
                if (editando != value)
                {
                    editando = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("MyProperty"));
                    }
                }
            }
            get { return editando; }
        }


        private string nome;
        public string Nome
        {
            set
            {
                if (nome != value)
                {
                    nome = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("Nome"));
                    }
                }
            }
            get { return nome; }
        }
        private DateTime nascimento;
        public DateTime Nascimento
        {
            set
            {
                if (nascimento != value)
                {
                    nascimento = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("Nascimento"));
                    }
                }
            }
            get { return nascimento; }
        }

        private Genero sexo;
        public Genero Sexo
        {
            set
            {
                if (sexo != value)
                {
                    sexo = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("Sexo"));
                    }
                }
            }
            get { return sexo; }
        }

        private string sobrenome;
        public string Sobrenome
        {
            set
            {
                if (sobrenome != value)
                {
                    sobrenome = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("Sobrenome"));
                    }
                }
            }
            get { return sobrenome; }
        }
        private string cpf;
        public string Cpf
        {
            set
            {
                if (cpf != value)
                {
                    cpf = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("Cpf"));
                    }
                }
            }
            get { return cpf; }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this,
                    new PropertyChangedEventArgs(propertyName));
            }
        }

        private Command cadastrarCommand;
        public ICommand CadastrarCommand
        {
            get
            {
                if (cadastrarCommand == null)
                {
                    cadastrarCommand = new Command(OnCadastrarClicked);
                }

                return cadastrarCommand;
            }
        }

        public async void OnCadastrarClicked(object param)
        {
            if (pessoaDTO == null)
            {
                pessoaDTO = new PessoaDTO();
                preencheEndereco(pessoaDTO);

                await model.InsertPessoaAsync(pessoaDTO);
            }
            else
            {
                preencheEndereco(pessoaDTO);

                await model.UpdatePessoaAsync(pessoaDTO);
            }
        }

        private void preencheEndereco(PessoaDTO pessoa)
        {
            pessoaDTO.Cpf = cpf;
            pessoaDTO.DataNascimento = nascimento;
            pessoaDTO.Nome = nome;
            pessoaDTO.Sexo = sexo;
            pessoaDTO.Sobrenome = sobrenome;
        }
    }
}
