﻿using MrCasting.Domain.DTO;
using MrCasting.Mobile.Model;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace MrCasting.Mobile.ViewModel
{
    public class InteresseViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        InteresseModel model = new InteresseModel();

        CandidatoDTO candidatoDTO;
        InteresseDTO interesseDTO;

        public InteresseViewModel()
        {

            candidatoDTO = (CandidatoDTO)NavigationHandler.Session["Candidato"];
            if (candidatoDTO == null)
            {
                candidatoDTO = new CandidatoDTO();
            }
            else
            {
                if (candidatoDTO.Interesse != null)
                {
                    interesseDTO = candidatoDTO.Interesse;
                }
            }

        }

        private bool _modelo;
        public bool Modelo
        {
            set
            {
                if (_modelo != value)
                {
                    _modelo = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("Modelo"));
                    }
                }
            }
            get { return _modelo; }
        }

        private bool _figurante;
        public bool Figurante
        {
            set
            {
                if (_figurante != value)
                {
                    _figurante = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("Figurante"));
                    }
                }
            }
            get { return _figurante; }
        }
        private bool _ator;
        public bool Ator
        {
            set
            {
                if (_ator != value)
                {
                    _ator = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("Ator"));
                    }
                }
            }
            get { return _ator; }
        }

        private bool _plussize;
        public bool PlusSize
        {
            set
            {
                if (_plussize != value)
                {
                    _plussize = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("PlusSize"));
                    }
                }
            }
            get { return _plussize; }
        }
        private bool _eventos;
        public bool Eventos
        {
            set
            {
                if (_eventos != value)
                {
                    _eventos = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("Eventos"));
                    }
                }
            }
            get { return _eventos; }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this,
                    new PropertyChangedEventArgs(propertyName));
            }
        }

        private Command avancarCommand;
        public ICommand AvancarCommand
        {
            get
            {
                if (avancarCommand == null)
                {
                    avancarCommand = new Command(OnAvancarClicked);
                }

                return avancarCommand;
            }
        }

        public async void OnAvancarClicked(object param)
        {
            //Implementar
            //Esse metodo deve direcionar a pessoa para a tela de cadastro das tags
        }
        private Command cadastrarCommand;
        public ICommand CadastrarCommand
        {
            get
            {
                if (cadastrarCommand == null)
                {
                    cadastrarCommand = new Command(OnCadastrarClicked);
                }

                return cadastrarCommand;
            }
        }

        public async void OnCadastrarClicked(object param)
        {
            interesseDTO = GetInteresses(candidatoDTO.Id);

            await model.InsertInteresseAsync(interesseDTO);
        }



        private InteresseDTO GetInteresses(int idCandidato)
        {
            InteresseDTO interessesDTO = new InteresseDTO();
            interessesDTO.IdCandidato = idCandidato;
            interessesDTO.Ator = _ator;
            interessesDTO.Evento = _eventos;
            interessesDTO.Figurante = _figurante;
            interessesDTO.Modelo = _modelo;
            interessesDTO.ModeloPlusSize = _plussize;

            return interessesDTO;
        }
    }
}
