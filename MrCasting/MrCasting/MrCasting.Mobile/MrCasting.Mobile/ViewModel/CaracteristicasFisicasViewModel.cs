﻿using MrCasting.Domain.DTO;
using MrCasting.Mobile.Model;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace MrCasting.Mobile.ViewModel
{
    public class CaracteristicasFisicasViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        CaracteristicasFisicasModel model = new CaracteristicasFisicasModel();

        public CaracteristicasFisicasViewModel()
        {
        }

        private string _corOlhos;
        public string CorOlhos
        {
            set
            {
                if (_corOlhos != value)
                {
                    _corOlhos = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("CorOlhos"));
                    }
                }
            }
            get { return _corOlhos; }
        }

        private string _corPele;
        public string CorPele
        {
            set
            {
                if (_corPele != value)
                {
                    _corPele = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("CorPele"));
                    }
                }
            }
            get { return _corPele; }
        }

        private decimal _manequim;
        public decimal Manequim
        {
            set
            {
                if (_manequim != value)
                {
                    _manequim = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("Manequim"));
                    }
                }
            }
            get { return _manequim; }
        }

        private decimal _cintura;
        public decimal Cintura
        {
            set
            {
                if (_cintura != value)
                {
                    _cintura = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("Cintura"));
                    }
                }
            }
            get { return _cintura; }
        }

        private decimal _busto;
        public decimal Busto
        {
            set
            {
                if (_busto != value)
                {
                    _busto = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("Busto"));
                    }
                }
            }
            get { return _busto; }
        }

        private decimal _quadril;
        public decimal Quadril
        {
            set
            {
                if (_quadril != value)
                {
                    _quadril = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("Quadril"));
                    }
                }
            }
            get { return _quadril; }
        }

        private decimal _sapato;
        public decimal Sapato
        {
            set
            {
                if (_sapato != value)
                {
                    _sapato = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("Sapato"));
                    }
                }
            }
            get { return _sapato; }
        }

        private string _comprimentoCabelo;
        public string ComprimentoCabelo
        {
            set
            {
                if (_comprimentoCabelo != value)
                {
                    _comprimentoCabelo = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("ComprimentoCabelo"));
                    }
                }
            }
            get { return _comprimentoCabelo; }
        }

        private string _tipoCabelo;
        public string TipoCabelo
        {
            set
            {
                if (_tipoCabelo != value)
                {
                    _tipoCabelo = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("TipoCabelo"));
                    }
                }
            }
            get { return _tipoCabelo; }
        }

        private string _etnia;
        public string Etnia
        {
            set
            {
                if (_etnia != value)
                {
                    _etnia = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("Etnia"));
                    }
                }
            }
            get { return _etnia; }
        }

        private string _descendencia;
        public string Descendencia
        {
            set
            {
                if (_descendencia != value)
                {
                    _descendencia = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("Descendencia"));
                    }
                }
            }
            get { return _descendencia; }
        }

        private decimal _peso;
        public decimal Peso
        {
            set
            {
                if (_peso != value)
                {
                    _peso = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("Peso"));
                    }
                }
            }
            get { return _peso; }
        }

        private string _tipoFisico;
        public string TipoFisico
        {
            set
            {
                if (_tipoFisico != value)
                {
                    _tipoFisico = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("TipoFisico"));
                    }
                }
            }
            get { return _tipoFisico; }
        }

        private string _camisa;
        public string Camisa
        {
            set
            {
                if (_camisa != value)
                {
                    _camisa = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("Camisa"));
                    }
                }
            }
            get { return _camisa; }
        }

        private decimal _torax;
        public decimal Torax
        {
            set
            {
                if (_torax != value)
                {
                    _torax = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("Torax"));
                    }
                }
            }
            get { return _torax; }
        }

        private string _terno;
        public string Terno
        {
            set
            {
                if (_terno != value)
                {
                    _terno = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("Terno"));
                    }
                }
            }
            get { return _terno; }
        }

        private decimal _altura;
        public decimal Altura
        {
            set
            {
                if (_altura != value)
                {
                    _altura = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("Altura"));
                    }
                }
            }
            get { return _altura; }
        }



        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this,
                    new PropertyChangedEventArgs(propertyName));
            }
        }

        private Command avancarCommand;
        public ICommand AvancarCommand
        {
            get
            {
                if (avancarCommand == null)
                {
                    avancarCommand = new Command(OnAvancarClicked);
                }

                return avancarCommand;
            }
        }

        public async void OnAvancarClicked(object param)
        {
            //Implementar
            //Esse metodo deve direcionar a pessoa para a tela de cadastro das tags
        }

        private Command cadastrarCommand;
        public ICommand CadastrarCommand
        {
            get
            {
                if (cadastrarCommand == null)
                {
                    cadastrarCommand = new Command(OnCadastrarClicked);
                }

                return cadastrarCommand;
            }
        }

        public async void OnCadastrarClicked(object param)
        {
            CaracteristicasFisicasDTO dto = new CaracteristicasFisicasDTO()
            {

                Altura = _altura,
                Busto = _busto,
                Camisa = _camisa,
                Cintura = _cintura,
                ComprimentoCabelo = _comprimentoCabelo,
                CorDaPele = _corPele,
                CorDosOlhos = _corOlhos,
                Descendencia = _descendencia,
                Etnia = _etnia,
                Manequim = _manequim,
                Peso = _peso,
                Quadril = _quadril,
                Sapato = _sapato,
                Terno = _terno,
                TipoCabelo = _tipoCabelo,
                TipoFisico = _tipoFisico,
                Torax = _torax
            };

            await model.InsertCaracteristicaAsync(dto);
        }
    }
}
