﻿using MrCasting.Domain.DTO;
using MrCasting.Mobile.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MrCasting.Mobile.ViewModel
{
    public class TagsViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        TagsModel model = new TagsModel();

        public TagsViewModel() { }

        private List<string> _tags;
        public List<string> Tags
        {
            set
            {
                if (_tags != value)
                {
                    _tags = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this,
                            new PropertyChangedEventArgs("Tags"));
                    }
                }
            }
            get { return _tags; }
        }
        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this,
                    new PropertyChangedEventArgs(propertyName));
            }
        }

        private Command avancarCommand;
        public ICommand AvancarCommand
        {
            get
            {
                if (avancarCommand == null)
                {
                    avancarCommand = new Command(OnAvancarClicked);
                }

                return avancarCommand;
            }
        }

        public async void OnAvancarClicked(object param)
        {
            //Implementar
            //Esse metodo deve direcionar a pessoa para a tela de cadastro das tags
        }


        private Command cadastrarCommand;
        public ICommand CadastrarCommand
        {
            get
            {
                if (cadastrarCommand == null)
                {
                    cadastrarCommand = new Command(OnCadastrarClicked);
                }

                return cadastrarCommand;
            }
        }

        public async void OnCadastrarClicked(object param)
        {
            TagsDTO dto = new TagsDTO()
            {
                List = Tags
            };
            await model.InsertTagsAsync(dto);
        }
    }


}
