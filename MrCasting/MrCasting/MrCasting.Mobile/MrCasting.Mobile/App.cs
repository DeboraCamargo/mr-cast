﻿using MrCasting.Domain.DTO;
using MrCasting.Mobile.Model;
using MrCasting.Mobile.View;

using Xamarin.Forms;

namespace MrCasting.Mobile
{
    public class App : Application
    {
        
        public App()
        {
            
        }

        protected override void OnStart()
        {

            NavigationHandler.SetUp(this);
            // NavigationHandler.Navigate(new CandidatoView());
            // NavigationHandler.Navigate(new CaracteristicasFisicasMulherView());
            //NavigationHandler.Navigate(new CaracteristicasFisicasHomemView());
            // NavigationHandler.Navigate(new TagsView());
            //NavigationHandler.Navigate(new HobbyView());
            //NavigationHandler.Navigate(new HabilidadeView());
            //NavigationHandler.Navigate(new InteresseView());

            PessoaDTO pessoa = new PessoaModel().GetPessoaAsync(1).Result;
            NavigationHandler.Session["Pessoa"] = pessoa;

            NavigationHandler.Navigate(new PessoaView());

            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
