﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MrCasting.Mobile.DTO
{
   public class ContatoPresentationDTO
    {
        public string Email { get; set; }
        public string Telefone { get; set; }
    }
}
