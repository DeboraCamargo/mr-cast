﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MrCasting.Mobile.DTO
{
    public class CaracteristicaFisicaPresentationDTO
    {
        public string CorOlhos { get; set; }
        public string CorPele { get; set; }
        public int Manequim { get; set; }
        public decimal Altura { get; set; }
        public decimal Cintura { get; set; }
        public decimal Busto { get; set; }
        public decimal Quadril { get; set; }
        public int Sapato { get; set; }
        public string ComprimentoCabelo { get; set; }
        public string TipoCabelo { get; set; }
        public string Etnia { get; set; }
        public string Descendencia { get; set; }
        public decimal Peso { get; set; }
        public string Terno { get; set; }
        public string Camisa { get; set; }
        public decimal Torax { get; set; }
        public string TipoFisico { get; set; }


    }
}
