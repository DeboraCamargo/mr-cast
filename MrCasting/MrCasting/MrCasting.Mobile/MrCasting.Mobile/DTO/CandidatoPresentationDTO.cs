﻿using System;
using System.Collections.Generic;

namespace MrCasting.Services.DTO
{
    public class CandidatoPresentationDTO
    {
        public int Id { get; set; }

        public string Profissao { get; set; }

        public List<string> Hobby { get; set; }

        public string Realise { get; set; }

        public string OrientacaoSexual { get; set; }

        public string DRT { get; set; }

        public List<string> Habilidade { get; set; }

        public string NomeArtistico { get; set; }

        public string SobrenomeArtistico { get; set; }

       public string Naturalidade { get; set; }

        public string Nacionalidade { get; set; }
    }
}