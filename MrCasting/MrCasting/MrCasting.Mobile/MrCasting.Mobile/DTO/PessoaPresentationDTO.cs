﻿using MrCasting.Domain.Enuns;
using System;

namespace MrCasting.Mobile.DTO
{
    public class PessoaPresentationDTO
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public string Sobrenome { get; set; }

        public DateTime DataNascimento { get; set; }

        public EnderecoPresentationDTO Endereco { get; set; }

        public string Cpf { get; set; }

        public ContatoPresentationDTO Contato { get; set; }

        public string Login { get; set; }

        public string Senha { get; set; }

        public string SenhaConfirmacao { get; set; }

        public Guid TokenAlteracaoDeSenha { get; set; }

        public Genero Sexo { get; set; }
   
    }
}
