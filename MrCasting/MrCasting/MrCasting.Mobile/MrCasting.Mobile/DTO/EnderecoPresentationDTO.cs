﻿namespace MrCasting.Mobile.DTO
{
    public class EnderecoPresentationDTO
    {

        public virtual string Logradouro { get; set; }

        public virtual string Complemento { get; set; }

        public virtual string Numero { get; set; }

        public virtual string Bairro { get; set; }

        public virtual string Cidade { get; set; }

        public virtual string Uf { get; set; }

        public virtual string Cep { get; set; }
    }
}
