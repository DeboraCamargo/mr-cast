﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace MrCasting.Mobile.View
{
    public partial class CandidatoView : ContentPage
    {
        public CandidatoView()
        {
            InitializeComponent();
            BindEvents();
        }

        private void BindEvents()
        {
            //Add events to TermoDeUso
            TapGestureRecognizer tapTermoUso = new TapGestureRecognizer();
            tapTermoUso.Tapped += termoUso_Tapped;
            TermosUsoLabel.GestureRecognizers.Add(tapTermoUso);

        }

        async void termoUso_Tapped(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new TermoDeUsoView());
        }
    }
}
