﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace MrCasting.Mobile
{
    class NavigationHandler
    {
        private static Application app;

        public static readonly IDictionary<string,object> Session = new Dictionary<string,object>();

        public  static void SetUp(Application application)
        {
            app = application;
            if (app == null)
                throw new Exception("Utilizar um applpication valido");
        }


        public static void Navigate(ContentPage view)
        {
            if (app == null)
                throw new Exception("Não utilizar esta classe sem antes usar o método setup.");
            app.MainPage = view;
        }
    }
}
