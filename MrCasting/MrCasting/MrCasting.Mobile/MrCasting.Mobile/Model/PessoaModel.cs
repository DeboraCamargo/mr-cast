﻿using MrCasting.Domain.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MrCasting.Mobile.Model
{
    class PessoaModel : ModelBase
    {
        public async Task<PessoaDTO> GetPessoaAsync(int idPessoa)
        {
            using (var client = GetHttpClient())
            {
                var response = await client.GetAsync(string.Format("api/Pessoa/{0}", idPessoa));
                var pessoaJson = response.EnsureSuccessStatusCode().Content.ReadAsStringAsync().Result;
                var pessoa = JsonConvert.DeserializeObject<PessoaDTO>(pessoaJson);

                return pessoa;
            }
        }

        public async Task InsertPessoaAsync(PessoaDTO pessoadto)
        {
            using (var client = GetHttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage();
                string jsonString = JsonConvert.SerializeObject(pessoadto);
                var response = await client.PostAsync("api/Pessoa", new StringContent(jsonString, Encoding.UTF8, "application/json"));
                var retorno = response.Content.ReadAsStringAsync().Result;
            }
        }

        public async Task UpdatePessoaAsync(PessoaDTO pessoadto)
        {
            using (var client = GetHttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage();
                string jsonString = JsonConvert.SerializeObject(pessoadto);
                var response = await client.PutAsync(string.Format("api/Pessoa/{0}",pessoadto.Id), new StringContent(jsonString, Encoding.UTF8, "application/json"));
                var retorno = response.Content.ReadAsStringAsync().Result;
            }
        }

    }
}
