﻿using System;
using System.Net.Http;

namespace MrCasting.Mobile.Model
{
    class ModelBase
    {
        protected HttpClient GetHttpClient()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://mrcasting.azurewebsites.net/");
            return client;
        }
    }
}
