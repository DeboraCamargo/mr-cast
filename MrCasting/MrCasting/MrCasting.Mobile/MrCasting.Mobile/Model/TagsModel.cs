﻿using MrCasting.Domain.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MrCasting.Mobile.Model
{
     class TagsModel : ModelBase
    {
        public async Task<TagsDTO> GetTagsAsync()
        {
            using (var client = GetHttpClient())
            {
                var response = await client.GetAsync("api/Tags/5");
                var tagsJson = response.EnsureSuccessStatusCode().Content.ReadAsStringAsync().Result;
                var tags = JsonConvert.DeserializeObject<TagsDTO>(tagsJson);

                return tags;
            }
        }



        public async Task InsertTagsAsync(TagsDTO tagsDTO)
        {
            using (var client = GetHttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage();
                string jsonString = JsonConvert.SerializeObject(tagsDTO);
                var response = await client.PostAsync("api/Tags", new StringContent(jsonString, Encoding.UTF8, "application/json"));
                var retorno = response.Content.ReadAsStringAsync().Result;
            }
        }

    }



}
