﻿using MrCasting.Domain.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MrCasting.Mobile.Model
{
    class EnderecoModel:ModelBase
    {
        private object contatoDTO;

        public async Task<EnderecoDTO> GetEnderecoAsync()
        {
            using (var client = GetHttpClient())
            {
                var response = await client.GetAsync("api/Endereco/5");
                var Enderecojson = response.EnsureSuccessStatusCode().Content.ReadAsStringAsync().Result;
                var endereco = JsonConvert.DeserializeObject<EnderecoDTO>(Enderecojson);

                return endereco;
            }
        }

        public async Task InsertEnderecoAsync(EnderecoDTO enderecoDTO)
        {
            using (var client = GetHttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage();
                string jsonString = JsonConvert.SerializeObject(enderecoDTO);
                var response = await client.PostAsync("api/Endereco", new StringContent(jsonString, Encoding.UTF8, "application/json"));
                var retorno = response.Content.ReadAsStringAsync().Result;
            }
        }

    }

}

    }
}
