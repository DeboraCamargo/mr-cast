﻿using MrCasting.Domain.DTO;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MrCasting.Mobile.Model
{
    class CandidatoModel : ModelBase
    {
        public async Task<CandidatoDTO> GetCandidatosAsync()
        {
            using (var client = GetHttpClient())
            {
                var response = await client.GetAsync("api/Candidato/5");
                var candidatoJson = response.EnsureSuccessStatusCode().Content.ReadAsStringAsync().Result;
                var candidato = JsonConvert.DeserializeObject<CandidatoDTO>(candidatoJson);

                return candidato;
            }
        }

        public async Task<CandidatoDTO> GetCandidatosAsync(int idCandidato)
        {
            using (var client = GetHttpClient())
            {
                var response = await client.GetAsync(string.Format("api/Candidato/{0}", idCandidato));
                var candidatoJson = response.EnsureSuccessStatusCode().Content.ReadAsStringAsync().Result;
                var candidato = JsonConvert.DeserializeObject<CandidatoDTO>(candidatoJson);

                return candidato;
            }
        }



        public async Task InsertCandidatosAsync(CandidatoDTO candidatoDTO)
        {
            using (var client = GetHttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage();
                string jsonString = JsonConvert.SerializeObject(candidatoDTO);
                var response = await client.PostAsync("api/Candidato", new StringContent(jsonString, Encoding.UTF8, "application/json"));
                var retorno = response.Content.ReadAsStringAsync().Result;
            }
        }

    }

   

}
