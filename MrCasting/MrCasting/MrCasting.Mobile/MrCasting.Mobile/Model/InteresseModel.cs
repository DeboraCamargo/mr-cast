﻿using MrCasting.Domain.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MrCasting.Mobile.Model
{
    class InteresseModel:ModelBase
    {
        public async Task<InteresseDTO> GetInteresseAsync()
        {
            using (var client = GetHttpClient())
            {
                var response = await client.GetAsync("api/Interesse/5");
                var interesseJson = response.EnsureSuccessStatusCode().Content.ReadAsStringAsync().Result;
                var interesse = JsonConvert.DeserializeObject<InteresseDTO>(interesseJson);

                return interesse;
            }
        }



        public async Task InsertInteresseAsync(InteresseDTO interesseDTO)
        {
            using (var client = GetHttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage();
                string jsonString = JsonConvert.SerializeObject(interesseDTO);
                var response = await client.PostAsync("api/Interesse", new StringContent(jsonString, Encoding.UTF8, "application/json"));
                var retorno = response.Content.ReadAsStringAsync().Result;
            }
        }

    }
}

