﻿using MrCasting.Domain.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MrCasting.Mobile.Model
{
    class HobbyModel :ModelBase
    {
        public async Task<HobbyDTO> GetHobbyAsync()
        {
            using (var client = GetHttpClient())
            {
                var response = await client.GetAsync("api/Hobby/5");
                var hobbyJson = response.EnsureSuccessStatusCode().Content.ReadAsStringAsync().Result;
                var hobby = JsonConvert.DeserializeObject<HobbyDTO>(hobbyJson);

                return hobby;
            }
        }



        public async Task InsertHobbyAsync(HobbyDTO hobbyDTO)
        {
            using (var client = GetHttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage();
                string jsonString = JsonConvert.SerializeObject(hobbyDTO);
                var response = await client.PostAsync("api/Hobby", new StringContent(jsonString, Encoding.UTF8, "application/json"));
                var retorno = response.Content.ReadAsStringAsync().Result;
            }
        }

    }

}
    
