﻿using MrCasting.Domain.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MrCasting.Mobile.Model
{
    class CaracteristicasFisicasModel : ModelBase
    {
        public async Task<CaracteristicasFisicasDTO> GetCaracteristicaAsync()
        {

            using (var client = GetHttpClient())
            {
                var response = await client.GetAsync("api/Candidato/5");
                var candidatoJson = response.EnsureSuccessStatusCode().Content.ReadAsStringAsync().Result;
                var candidato = JsonConvert.DeserializeObject<CaracteristicasFisicasDTO>(candidatoJson);

                return candidato;
            }
        }

        public async Task InsertCaracteristicaAsync(CaracteristicasFisicasDTO caracteristicasFisicasDTO)
        {
            using (var client = GetHttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage();
                string jsonString = JsonConvert.SerializeObject(caracteristicasFisicasDTO);
                var response = await client.PostAsync("api/Candidato", new StringContent(jsonString, Encoding.UTF8, "application/json"));
                var retorno = response.Content.ReadAsStringAsync().Result;
            }
        }


    }
}
