﻿using MrCasting.Domain.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MrCasting.Mobile.Model
{
    class HabilidadeModel : ModelBase
    {
        public async Task<HabilidadeDTO> GetHabilidadeAsync()
        {
            using (var client = GetHttpClient())
            {
                var response = await client.GetAsync("api/Habilidade/5");
                var habilidadeJson = response.EnsureSuccessStatusCode().Content.ReadAsStringAsync().Result;
                var habilidade = JsonConvert.DeserializeObject<HabilidadeDTO>(habilidadeJson);

                return habilidade;
            }
        }



        public async Task InsertHabilidadeAsync(HabilidadeDTO habilidadeDTO)
        {
            using (var client = GetHttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage();
                string jsonString = JsonConvert.SerializeObject(habilidadeDTO);
                var response = await client.PostAsync("api/Hobby", new StringContent(jsonString, Encoding.UTF8, "application/json"));
                var retorno = response.Content.ReadAsStringAsync().Result;
            }
        }

    }
}
