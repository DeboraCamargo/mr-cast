﻿using MrCasting.Domain.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MrCasting.Mobile.Model
{
    class ContatoModel:ModelBase
    {
        public async Task<ContatoDTO> GetContatoAsync()
        {
            using (var client = GetHttpClient())
            {
                var response = await client.GetAsync("api/Contato/5");
                var contatoJson = response.EnsureSuccessStatusCode().Content.ReadAsStringAsync().Result;
                var contato = JsonConvert.DeserializeObject<ContatoDTO>(contatoJson);

                return contato;
            }
        }



        public async Task InsertContatoAsync(ContatoDTO contatoDTO)
        {
            using (var client = GetHttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage();
                string jsonString = JsonConvert.SerializeObject(contatoDTO);
                var response = await client.PostAsync("api/Contato", new StringContent(jsonString, Encoding.UTF8, "application/json"));
                var retorno = response.Content.ReadAsStringAsync().Result;
            }
        }

    }

}
