﻿angular.module('facebook', ['ezfb', 'hljs'])
.config(function (ezfbProvider) {
    ezfbProvider.setInitParams({
        appId: '789047904572860'
    });  
})
.controller('FacebookController', function ($scope, ezfb, $window, $location, $state) {
  
    updateLoginStatus(updateApiMe);

    $scope.login = function () {
        ezfb.login(function (res) {
            if (res.authResponse) {
                //Sucesso ?
                updateLoginStatus(updateApiMe);
                $state.go('app.editarDadosPessoais');
            } else {
                $state.go('termos');
            }
        }, {scope: 'email'});
    };

    $scope.logout = function () {
        ezfb.logout(function () {
            updateLoginStatus(updateApiMe);
        });
    };

    //$scope.share = function () {
    //    ezfb.ui(
    //      {
    //          method: 'feed',
    //          name: 'angular-easyfb API demo',
    //          picture: 'http://plnkr.co/img/plunker.png',
    //          link: 'http://plnkr.co/edit/qclqht?p=preview',
    //          description: 'angular-easyfb is an AngularJS module wrapping Facebook SDK.' + 
    //                       ' Facebook integration in AngularJS made easy!' + 
    //                       ' Please try it and feel free to give feedbacks.'
    //      },
    //      function (res) {
    //          // res: FB.ui response
    //      }
    //    );
    //};

    
    function updateLoginStatus (more) {
        ezfb.getLoginStatus(function (res) {
            $scope.loginStatus = res;

            (more || angular.noop)();
        });
    }

    function updateApiMe () {
        ezfb.api('/me', function (res) {
            $scope.apiMe = res;
        });
    }
});
